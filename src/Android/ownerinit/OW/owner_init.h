#ifndef OWNER_INIT_H
#define OWNER_INIT_H
 
int owner_init_init (void * sub_proc, void * para);
int owner_init_start (void * sub_proc, void * para);
#endif

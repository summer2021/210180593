#ifndef USER_SERVER_H
#define USER_SERVER_H
 
 
int hub_server_init (void * sub_proc, void * para);
int hub_server_start (void * sub_proc, void * para);
#endif

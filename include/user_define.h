enum dtype_user_define {
	TYPE(USER_DEFINE)=0x3200,
};

enum subtype_user_define {
	SUBTYPE(USER_DEFINE,INIT)=0x1,
	SUBTYPE(USER_DEFINE,RETURN)
};

enum enum_init_state
{
	SUCCEED=0x01,
	INVALID,
	NOUSER,
	AUTHFAIL,
	NOACCESS
};

typedef struct user_define_init{
	BYTE uuid[DIGEST_SIZE];
	BYTE prikey_data[DIGEST_SIZE];
}__attribute__((packed)) RECORD(USER_DEFINE,INIT);

typedef struct user_define_return{
	UINT32 return_code;
	char * return_info;
}__attribute__((packed)) RECORD(USER_DEFINE,RETURN);
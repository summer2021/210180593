enum dtype_hub_define {
	TYPE(HUB)=0x4510
};
enum subtype_hub_define {
	SUBTYPE(HUB,HUB_DATA)=0x1,
	SUBTYPE(HUB,LOCKER_DATA),
};

typedef struct record_define_hub_data{
	BYTE uuid[DIGEST_SIZE];
	BYTE * hub_location;
	BYTE hub_owner[DIGEST_SIZE];
    BYTE pubkey_data[DIGEST_SIZE];
}__attribute__((packed)) RECORD(HUB,HUB_DATA);

typedef struct record_define_lock_data{
	BYTE uuid[DIGEST_SIZE];
    BYTE hub_uuid[DIGEST_SIZE];
	BYTE * lock_location;
}__attribute__((packed)) RECORD(HUB,LOCK_DATA);

enum dtype_owner_define {
	TYPE(OWNER)=0x4500
};
enum subtype_owner_define {
	SUBTYPE(OWNER,OWNER_DATA)=0x1,
	SUBTYPE(OWNER,LOCK_DATA),
    SUBTYPE(OWNER,GUEST_DATA),
};

typedef struct record_define_owner_data{
	BYTE uuid[DIGEST_SIZE];
	BYTE * user_name;
	BYTE passwd[DIGEST_SIZE];
    BYTE prikey_data[DIGEST_SIZE];
}__attribute__((packed)) RECORD(OWNER,OWNER_DATA);

typedef struct record_define_lock_data{
	BYTE uuid[DIGEST_SIZE];
	INT lock_num;
	BYTE * lock_list;
	INT visit_num;
	BYTE * visit_list;
}__attribute__((packed)) RECORD(OWNER,LOCK_DATA);

typedef struct record_define_guest_data{
	BYTE uuid[DIGEST_SIZE];
	INT num;
	BYTE * guest_list;
}__attribute__((packed)) RECORD(OWNER,GUEST_DATA);

enum dtype_token_define {
	TYPE(TOKEN)=0x4520
};

enum subtype_token_define {
	SUBTYPE(TOKEN,AUTHOR_DATA)=0x1,
	SUBTYPE(TOKEN,PROVE_DATA),
};

typedef struct record_define_author_data{
	BYTE owner_uuid[DIGEST_SIZE];
    BYTE guest_uuid[DIGEST_SIZE];
    BYTE lock_uuid[DIGEST_SIZE];
    BYTE * time;
    BYTE signture[DIGEST_SIZE];
    BYTE encrypt[DIGEST_SIZE];
}__attribute__((packed)) RECORD(TOKEN,AUTHOR_DATA);

typedef struct record_define_prove_data{
	BYTE owner_uuid[DIGEST_SIZE];
    BYTE guest_uuid[DIGEST_SIZE];
    BYTE lock_uuid[DIGEST_SIZE];
    BYTE * time;
    BYTE guest_pubkey[DIGEST_SIZE];
    BYTE signature[DIGEST_SIZE];
    BYTE encrypt[DIGEST_SIZE];
}__attribute__((packed)) RECORD(TOKEN,PROVE_DATA);

enum dtype_unlock_define {
	TYPE(UNLOCK)=0x4540;
};

enum subtype_unlock_define {
	SUBTYPE(UNLOCK,PROCESS)=0x1,
};

typedef struct record_define_unlocking_process{
    int unlock_request;
    BYTE challenge_info[DIGEST_SIZE];
    BYTE reply_info[DIGEST_SIZE];
    BYTE confirm_info[DIGEST_SIZE];
}__attribute__((packed)) RECORD(UNLOCK,PROCESS);

enum dtype_audit_define {
	TYPE(AUDIT)=0x4530;
};

enum subtype_audit_define {
	SUBTYPE(AUDIT,REQUEST)=0x1,
    SUBTYPE(AUDIT,REPORT),
};

typedef struct record_define_audit_request{
    BYTE * audit_data_request;
    BYTE * requestor_info;
    BYTE * auditor_info;
    BYTE auditor_signature[DIGEST_SIZE];
}__attribute__((packed)) RECORD(AUDIT,REQUEST);

typedef struct record_define_audit_report{
    BYTE data_index[DIGEST_SIZE];
    BYTE * report_attribute;
    BYTE report_signature[DIGEST_SIZE];
}__attribute__((packed)) RECORD(AUDIT,REPORT);